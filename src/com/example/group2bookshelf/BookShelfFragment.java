package com.example.group2bookshelf;

import android.app.Fragment;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

public class BookShelfFragment extends Fragment {
	
	public boolean onCreateOptionsMenu(Menu menu) {
	    getMenuInflater().inflate(R.menu.main, menu);
	    return true;
	}

	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_add:
                // Toast.makeText(this, "Add", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), addBook.class));
                return true;
            case R.id.menu_list:
                // Toast.makeText(this, "List", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), Book.class));
                return true;
            case R.id.menu_menu:
                // Toast.makeText(this, "Menu", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), menuFragment.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

