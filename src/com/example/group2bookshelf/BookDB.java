package com.example.group2bookshelf;


import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class BookDB
{
    // Database constants
    public static final String DB_NAME = "book.db";
    public static final int    DB_VERSION = 1;

    // Book table constants
    public static final String BOOK_TABLE = "book";

    public static final String BOOK_ID = "_id";
    public static final int    BOOK_ID_COL = 0;

    public static final String BOOK_TITLE = "title";
    public static final int    BOOK_TITLE_COL = 1;

    public static final String BOOK_AUTHOR = "author";
    public static final int    BOOK_AUTHOR_COL = 2;

    public static final String BOOK_SERIES = "series";
    public static final int    BOOK_SERIES_COL = 3;

    public static final String BOOK_PUBLISHER = "publisher";
    public static final int    BOOK_PUBLISHER_COL = 4;

    public static final String BOOK_ISBN = "isbn";
    public static final int    BOOK_ISBN_COL = 5;

    public static final String BOOK_PRICE = "price";
    public static final int    BOOK_PRICE_COL = 6;

    public static final String BOOK_OWNED = "owned";
    public static final int    BOOK_OWNED_COL = 7;


    // CREATE and DROP TABLE statements
    public static final String CREATE_BOOK_TABLE = "CREATE TABLE " + BOOK_TABLE + " (" +
            BOOK_ID   		+ " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            BOOK_TITLE 		+ " TEXT    NOT NULL, " +
            BOOK_AUTHOR 	+ " TEXT, " +
            BOOK_SERIES 	+ " TEXT, " +
            BOOK_PUBLISHER	+ " TEXT, " +
            BOOK_ISBN		+ " TEXT, " +
            BOOK_PRICE		+ " TEXT, " +
            BOOK_OWNED		+ " TEXT);";


    public static final String DROP_BOOK_TABLE = "DROP TABLE IF EXISTS " + BOOK_TABLE;

    private static class DBHelper extends SQLiteOpenHelper
    {
        public DBHelper(Context context, String name, CursorFactory factory, int version)
        {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db)
        {
            // Create tables
            db.execSQL(CREATE_BOOK_TABLE);

            // Insert default books
            db.execSQL("INSERT INTO book VALUES (1, 'Murach's Android Programming', 'Joel Murach', " +
            "'Training Reference', 'Mike Murach', '9781890774714', '52.99', 'Yes')");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            Log.d("Book ", "Upgrading db from version " + oldVersion + " to " + newVersion);

            db.execSQL(BookDB.DROP_BOOK_TABLE);
            onCreate(db);
        }
    }

    // Database and database helper objects
    private SQLiteDatabase db;
    private DBHelper dbHelper;

    // Constructor
    public BookDB(Context context)
    {
        dbHelper = new DBHelper(context, DB_NAME, null, DB_VERSION);
    }

    // private methods
    private void openReadableDB()
    {
        db = dbHelper.getReadableDatabase();
    }

    private void openWriteableDB() {
        db = dbHelper.getWritableDatabase();
    }

    private void closeDB()
    {
        if (db != null)
            db.close();
    }

    // public methods
    public ArrayList<Book> getBooks()
    {
        ArrayList<Book> books = new ArrayList<Book>();
        openReadableDB();
        Cursor cursor = db.query(BOOK_TABLE, null, null, null, null, null, null);
        while (cursor.moveToNext())
        {
             Book book = new Book();
             book.setBookId(cursor.getInt(BOOK_ID_COL));
             book.setTitle(cursor.getString(BOOK_TITLE_COL));
             book.setAuthor(cursor.getString(BOOK_AUTHOR_COL));
             book.setSeries(cursor.getString(BOOK_SERIES_COL));
             book.setPublisher(cursor.getString(BOOK_PUBLISHER_COL));
             book.setIsbn(cursor.getString(BOOK_ISBN_COL));
             book.setPrice(cursor.getDouble(BOOK_PRICE_COL));
             book.setOwned(cursor.getString(BOOK_OWNED_COL));

             books.add(book);
        }
        if (cursor != null)
            cursor.close();
        closeDB();

        return books;
    }  

    public Book getBook(int id)
    {
        String where = BOOK_ID + "= ?";
        String[] whereArgs = { Integer.toString(id) };

        this.openReadableDB();
        Cursor cursor = db.query(BOOK_TABLE, null, where, whereArgs, null, null, null);
        cursor.moveToFirst();
        Book book = getBookFromCursor(cursor);
        if (cursor != null)
            cursor.close();
        this.closeDB();

        return book;
    }

    private static Book getBookFromCursor(Cursor cursor)
    {
        if (cursor == null || cursor.getCount() == 0)
        {
            return null;
        }
        else
        {
            try
            {
                Book book = new Book(cursor.getInt(BOOK_ID_COL),
                					 cursor.getString(BOOK_TITLE_COL),
                					 cursor.getString(BOOK_AUTHOR_COL),
                					 cursor.getString(BOOK_SERIES_COL),
                					 cursor.getString(BOOK_PUBLISHER_COL),
                					 cursor.getString(BOOK_ISBN_COL),
                					 cursor.getDouble(BOOK_PRICE_COL),
                					 cursor.getString(BOOK_OWNED_COL));
                return book;
            }
            catch(Exception e)
            {
                return null;
            }
        }
    }

    public long insertBook(Book book)
    {
        ContentValues cv = new ContentValues();
        cv.put(BOOK_ID, book.getBookId());
        cv.put(BOOK_TITLE, book.getTitle());
        cv.put(BOOK_AUTHOR, book.getAuthor());
        cv.put(BOOK_SERIES, book.getSeries());
        cv.put(BOOK_PUBLISHER, book.getPublisher());
        cv.put(BOOK_ISBN, book.getIsbn());
        cv.put(BOOK_PRICE, book.getPrice());
        cv.put(BOOK_OWNED, book.isOwned());

        this.openWriteableDB();
        long rowID = db.insert(BOOK_TABLE, null, cv);
        this.closeDB();

        return rowID;
    }

    public int updateBook(Book book)
    {
        ContentValues cv = new ContentValues();
        cv.put(BOOK_ID, book.getBookId());
		cv.put(BOOK_TITLE, book.getTitle());
		cv.put(BOOK_AUTHOR, book.getAuthor());
		cv.put(BOOK_SERIES, book.getSeries());
		cv.put(BOOK_PUBLISHER, book.getPublisher());
		cv.put(BOOK_ISBN, book.getIsbn());
		cv.put(BOOK_PRICE, book.getPrice());
        cv.put(BOOK_OWNED, book.isOwned());

        String where = BOOK_ID + "= ?";
        String[] whereArgs = { String.valueOf(book.getBookId()) };

        this.openWriteableDB();
        int rowCount = db.update(BOOK_TABLE, cv, where, whereArgs);
        this.closeDB();

        return rowCount;
    }

    public int deleteBook(long id)
    {
        String where = BOOK_ID + "= ?";
        String[] whereArgs = { String.valueOf(id) };

        this.openWriteableDB();
        int rowCount = db.delete(BOOK_TABLE, where, whereArgs);
        this.closeDB();

        return rowCount;
    }
}