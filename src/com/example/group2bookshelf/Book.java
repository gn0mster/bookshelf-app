package com.example.group2bookshelf;

public class Book
{
    private int bookId;
    private String title;
    private String author;
    private String series;
    private String publisher;
    private String isbn;
    private double price;
    private String owned;

    public Book() {}

    public long getBookId()
    {
		return bookId;
	}

	public void setBookId(int bookId)
	{
		this.bookId = bookId;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getAuthor()
    {
		return author;
	}

	public void setAuthor(String author)
	{
		this.author = author;
	}

	public String getSeries()
	{
		return series;
	}

	public void setSeries(String series)
	{
		this.series = series;
	}

	public String getPublisher()
	{
		return publisher;
	}

	public void setPublisher(String publisher)
	{
		this.publisher = publisher;
	}

	public String getIsbn()
	{
		return isbn;
	}

	public void setIsbn(String isbn)
	{
		this.isbn = isbn;
	}

	public double getPrice()
	{
		return price;
	}

	public void setPrice(double price)
	{
		this.price = price;
	}

	public String isOwned()
	{
		return owned;
	}
	public void setOwned(String owned)
	{
		this.owned = owned;
	}

	// A constructor

	public Book(int bookId, String title, String author, String series, String publisher,
				String isbn, double price, String owned)
	{
		this.bookId= bookId;
		this.title = title;
		this.author = author;
		this.series = series;
		this.publisher = publisher;
		this.isbn = isbn;
		this.price = price;
		this.owned = owned;
	}
}