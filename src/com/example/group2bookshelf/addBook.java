package com.example.group2bookshelf;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;


public class addBook extends Activity 
implements OnKeyListener {
	    
	    private TextView add_FragmentTitle;
	    private TextView add_lblTitle;
        private EditText add_txtTitle;
        private TextView add_lblAuthor;
        private EditText add_txtAuthor;
        private TextView add_lblSeries;
        private EditText add_txtSeries;
        private TextView add_lblPublisher;
        private EditText add_txtPublisher;
        private TextView add_lblISBN;
        private EditText add_txtISB;
        
        private Spinner listSpinner;

	    private BookDB db;
	    private boolean editMode;
	    private String currentTabName = "";
	    private Book book;


    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);        
	        setContentView(R.layout.fragment_add);
	        
	        // get references to widgets
        listSpinner = (Spinner) findViewById(R.id.listSpinner);
        add_FragmentTitle = (TextView) findViewById(R.id.add_FragmentTitle);
        add_lblTitle = (TextView) findViewById(R.id.add_lblTitle);
        add_txtTitle = (EditText) findViewById(R.id.add_txtTitle);
        add_txtAuthor = (EditText) findViewById(R.id.add_txtAuthor);
	        
	        // set listeners
        add_FragmentTitle.setOnKeyListener(this);
        add_lblTitle.setOnKeyListener(this);
        add_txtTitle.setOnKeyListener(this);
        add_txtAuthor.setOnKeyListener(this);

	        // get the database object
	        db = new BookDB(this);

        // get edit mode from intent
        Intent intent = getIntent();
        editMode = intent.getBooleanExtra("editMode", false);

        // if editing
        if (editMode) {
            // get book
            long bookId = intent.getLongExtra("bookId", -1);
            book = db.getBook(bookId);

            // update UI with task
            nameEditText.setText(task.getName());
            notesEditText.setText(task.getNotes());
        }

        // set the correct list for the spinner
        long listID;
        if (editMode) {   // edit mode - use same list as selected task
            listID = (int) task.getListId();
        }
        else {            // add mode - use the list for the current tab
            currentTabName = intent.getStringExtra("tab");
            listID = (int) db.getList(currentTabName).getId();
        }
        // subtract 1 from database ID to get correct list position
        int listPosition = (int) listID - 1;
        listSpinner.setSelection(listPosition);
    }

	    @Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	        getMenuInflater().inflate(R.menu.activity_add_edit, menu);
	        return true;
	    }
	    
	    @Override
	    public boolean onOptionsItemSelected(MenuItem item) {
	        switch (item.getBookId()){
	            case R.id.menuSave:
	                saveToDB();
	                this.finish();
	                break;
	            case R.id.menuCancel:
	                this.finish();
	                break;
	        }
	        return super.onOptionsItemSelected(item);
	    }
	    
	    private void saveToDB() {
	        // get data from widgets
	        String name = nameEditText.getText().toString();
	        String notes = notesEditText.getText().toString();
	        
	        // if no task name, exit method
	        if (name == null || name.equals("")) {
	            return;
	        }
	        
	        // if add mode, create new task
	        if (!editMode) {
	            book = new Book();
	        }
	        
	        // put data in task
            book.setBookId(bookID);
	        book.setTitle(name);
	        book.setNotes(notes);
	        
	        // update or insert task
	        if (editMode) {
	            db.updateBook(book);
	        }
	        else {
	            db.insertBook(book);
	        }
	    }

	    @Override
	    public boolean onKey(View view, int keyCode, KeyEvent event) {
	        if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
	            // hide the soft Keyboard
	            InputMethodManager imm = (InputMethodManager) 
	                    getSystemService(Context.INPUT_METHOD_SERVICE);
	            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	            return true;
	        }
	        else if (keyCode == KeyEvent.KEYCODE_BACK) {
	            saveToDB();
	            return false;
	        }
	        return false;
	    }
	}